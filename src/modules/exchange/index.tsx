import { useContext, useReducer } from "react";
import { Select } from "components/Select";
import { ScExchange, ScInputContainer, ScIcon, ScSelectPlug } from "./styled";
import { ReactComponent as Icon } from "assets/images/exchange.svg";
import { InputType } from "components/Input";
import { Currency, CurrencyContext } from "context/currency";
import reducer, { ReducerTypes, ValueKeys } from "helpers/reducer";
import { ScInput } from "components/Input/styled";
import { defaultValue } from "context/currency";

interface ExhangeProps {
  array: Currency[];
  selectedCurrency: Currency;
}
export const Exhange = ({ array, selectedCurrency }: ExhangeProps) => {
  const [state, dispatch] = useReducer(reducer, {
    valueReference: "",
    values: {
      value1: "",
      value2: "",
    },
    selectedCurrencyRate: selectedCurrency,
  });

  const onUpdate = (
    name: string,
    valueReference: ValueKeys,
    value: string,
    selectedCurrencyRate: Currency
  ) => {
    dispatch({
      type: ReducerTypes.SET_VALUE,
      payload: {
        name,
        valueReference,
        value,
        selectedCurrencyRate,
      },
    });
  };

  return (
    <ScExchange>
      <ScInputContainer>
        <div className="headline">If you have:</div>
        <div className="inputLine">
          <ScInput
            onChange={(e: any) =>
              onUpdate(
                "value1",
                ValueKeys.Value2,
                e.target.value,
                selectedCurrency
              )
            }
            type={InputType.Number}
            value={state.values.value1}
            min="0"
          />
          <Select
            maxWidth={120}
            currency={array}
            value={state.selectedCurrencyRate}
          />
        </div>
        <div className="rate">
          The average exchange rate of {"1 UAH"} ={" "}
          {(1 / Number(selectedCurrency.rate)).toFixed(2)} {selectedCurrency.cc}
        </div>
      </ScInputContainer>
      <ScIcon>
        <Icon />
      </ScIcon>
      <ScInputContainer>
        <div className="headline">You will get:</div>
        <div className="inputLine">
          <ScInput
            onChange={(e: any) =>
              onUpdate(
                "value2",
                ValueKeys.Value1,
                e.target.value,
                selectedCurrency
              )
            }
            type={InputType.Number}
            value={state.values.value2}
            min="0"
          />
          <ScSelectPlug>
            <span>uah</span>
            <img className="flag" src={defaultValue.flag} alt="ua" />
          </ScSelectPlug>
        </div>
        <div className="rate">
          The average exchange rate of {`1 ${selectedCurrency.cc}`} ={" "}
          {`${selectedCurrency.rate.toFixed(2)} UAH`}
        </div>
      </ScInputContainer>
    </ScExchange>
  );
};
