import { ScSelectedOption } from "components/Select/styled";
import styled from "styled-components/macro";
import { theme } from "theme";

export const ScExchange = styled.div`
  display: flex;
  margin: 0 0 20px;
  position: relative;
  justify-content: space-between;
`;

export const ScInputContainer = styled.div`
  flex: 0 0 calc(50% - 10px);
  max-width: calc(50% - 10px);
  background-color: ${theme.whiteColor};
  border-radius: 10px;
  padding: 26px 28px;
  box-sizing: border-box;

  .headline {
    font-weight: 500;
    font-size: 16px;
    line-height: 16px;
    margin: 0 0 23px 0;
  }

  .inputLine {
    display: flex;
    justify-content: space-between;
    position: relative;
    margin-bottom: 28px;
  }

  .rate {
    font-weight: 400;
    font-size: 14px;
    line-height: 14px;
    opacity: 0.5;
  }
`;

export const ScIcon = styled.div`
  position: absolute;
  left: 50%;
  top: 50%;
  translate: -50% -50%;
  padding: 14px;
  background-color: ${theme.whiteColor};
  border-radius: 8px;
  box-shadow: ${theme.shadow};
  width: 48px;
  height: 48px;
  box-sizing: border-box;
`;

export const ScSelectPlug = styled(ScSelectedOption)`
  max-width: 120px;
  cursor: not-allowed;
`;
