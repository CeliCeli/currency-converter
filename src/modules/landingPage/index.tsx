import { useContext, useMemo } from "react";
import { CurrencyContext, CurrenciesEnum } from "context/currency";
import { Exhange } from "../exchange/";
import { Header } from "components/Header";
import { ScContainer, ScMain, ScAside, ScBlock } from "styles/general";
import { Table, TableArray } from "components/Table";

export const Landing = () => {
  const { array, selectedCurrency } = useContext(CurrencyContext);

  const tableArray: TableArray[] = useMemo(() => {
    return array
      .filter((obj) => obj.cc !== CurrenciesEnum.UAH)
      .map((obj) => [
        { img: obj.flag },
        obj.cc.toString(),
        (obj.rate / selectedCurrency.rate).toFixed(2).toString(),
        ((selectedCurrency.rate / obj.rate) * 100).toFixed(2).toString(),
      ]);
  }, [array, selectedCurrency]);

  const headerArray = useMemo(() => {
    return array
      .filter(
        (obj) => obj.cc === CurrenciesEnum.USD || obj.cc === CurrenciesEnum.EUR
      )
      .map((obj) => [obj.cc, obj.rate.toFixed(2).toString()]);
  }, [array]);

  return (
    <>
      <Header date={""} array={headerArray} />
      <ScContainer>
        <h1>Currency Converter</h1>
        <ScMain>
          <Exhange array={array} selectedCurrency={selectedCurrency} />
          <ScBlock>
            <h2>All currency calculator rates:</h2>
            <Table
              data={{
                headers: ["", "Currency", "Rate", `100 ${selectedCurrency.cc}`],
                array: tableArray,
              }}
            />
          </ScBlock>
        </ScMain>
        <ScAside />
      </ScContainer>
    </>
  );
};
