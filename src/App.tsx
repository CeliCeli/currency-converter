import { NavigationContainer } from "@react-navigation/native";
import { CurrencyProvider } from "context/currency";
import { Landing } from "./modules/landingPage";
import GlobalStyle from "./styles/global";
import { theme } from "./theme";

function App() {
  return (
    <NavigationContainer theme={theme}>
      {/* <ErrorBoundary> */}
      <CurrencyProvider>
        <GlobalStyle />
        <Landing />
      </CurrencyProvider>
      {/* </ErrorBoundary> */}
    </NavigationContainer>
  );
}

export default App;
