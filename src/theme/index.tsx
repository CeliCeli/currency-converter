import { DefaultTheme } from "@react-navigation/native";

export const theme = {
  ...DefaultTheme,
  whiteColor: "#ffffff",
  primaryColor: "#5ECC8A",
  parimaryHover: "#169466",
  secondaryColor1: "#1A170D",
  secondaryColor2: "#F3F5FB",
  shadow: "0px 10px 20px rgba(22, 27, 43, 0.1);",
};
