import {
    createGlobalStyle
} from 'styled-components';
import {
    theme
} from 'theme';

const GlobalStyle = createGlobalStyle `
    @import url('https://fonts.googleapis.com/css2?family=Epilogue:wght@400;500;600;700&display=swap');

    body {
        margin: 0;
        padding: 0;
        font-family: 'Epilogue', sans-serif;
        background: ${theme.secondaryColor2}
    } 

    *{
        font-family: 'Epilogue', sans-serif;
    }

    img{
        max-width: 100%;
        vertical-align: bottom;
    }

    h1,h2,h3,h4,h5{
        margin: 0;
        padding: 0;
    }

    h1{
        font-weight: 700;
        font-size: 32px;
        line-height: 33px;
    }

    p{
        margin: 0;
    }

    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
    }

    input[type=number] {
    -moz-appearance: textfield;
    }

`;

export default GlobalStyle;