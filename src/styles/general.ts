import styled from "styled-components/macro";
import { theme } from "theme";

export const ScContainer = styled.div`
  max-width: 1240px;
  padding: 0 16px;
  margin: 0 auto;
  display: flex;
  justify-content: space-between;
  align-items: stretch;
  flex-wrap: wrap;

  h1 {
    flex: 0 0 100%;
    max-width: 100%;
    margin-bottom: 28px;
  }

  h2 {
    font-weight: 600;
    font-size: 20px;
    line-height: 20px;
    margin-bottom: 24px;
  }
`;

export const ScBlock = styled.div`
  background-color: ${theme.whiteColor};
  border-radius: 10px;
  padding: 28px;
`;

export const ScMain = styled.div`
  flex: 0 0 calc(66.6% - 10px);
  max-width: calc(66.6% - 10px);
`;

export const ScAside = styled(ScBlock)`
  flex: 0 0 calc(33.3% - 10px);
  max-width: calc(33.3% - 10px);
  box-sizing: border-box;
  min-height: 300px;
`;


// media queries there