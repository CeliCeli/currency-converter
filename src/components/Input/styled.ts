import styled from "styled-components/macro";
import { theme } from "theme";

export const ScInput = styled.input`
  border: none;
  background-color: transparent;
  outline: none;
  font-weight: 600;
  font-size: 40px;
  line-height: 41px;
  width: 100%;
  &::placeholder{
    color: ${theme.secondaryColor1}
  }
`;
