import { FC } from "react";
import { ScInput } from "./styled";

export const enum InputType {
  Text = "text",
  Number = "number",
}

type InputProps = HTMLInputElement & {
  value?: number | string;
  type?: InputType;
  pattern?: string;
  placeholder?: string;
};

export const Input: FC<InputProps> = ({
  value,
  type,
  pattern,
  placeholder,
}) => {
  return (
    <ScInput
      type={type ?? InputType.Text}
      placeholder={placeholder ?? "...type"}
      pattern={pattern}
      value={value}
    />
  );
};
