import { FC, useMemo } from "react";
import { ScTable, ScTableBody, ScTableHead } from "./styled";

export type TableArray = [
  {
    img: string;
  },
  ...any[]
];

type Data = {
  headers: string[];
  array: TableArray[];
};

interface TableProps {
  data: Data;
}
export const Table = ({ data }: TableProps) => {
  const { headers, array } = useMemo(() => {
    const headers = data.headers;
    const array = data.array;
    return { headers, array };
  }, [data]);

  return (
    <ScTable>
      <ScTableHead>
        <tr>
          {headers.map((el, index) => (
            <th key={index}>{el}</th>
          ))}
        </tr>
      </ScTableHead>
      <ScTableBody>
        {array.map((arr, index) => (
          <tr key={index}>
            {arr.map((el, index) => (
              <td key={index}>
                {typeof el !== "string" ? <img src={el.img} /> : el}
              </td>
            ))}
          </tr>
        ))}
      </ScTableBody>
    </ScTable>
  );
};
