import styled, { css } from "styled-components/macro";
import { theme } from "theme";

const CurrencyTable = css`
  th,
  td {
    width: 25%;
    text-align: center;

    &:first-child {
      text-align: left;
    }
    &:last-child {
      text-align: right;
    }

    img {
      object-fit: cover;
      width: 44px;
      height: 24px;
      border: 1px solid ${theme.secondaryColor2};
      border-radius: 4px;
    }
  }
`;

export const ScTable = styled.table`
  width: 100%;
  ${CurrencyTable};
  th,
  td {
    border-bottom: 1px solid ${theme.secondaryColor2};
    padding: 16px 0;
  }
`;

export const ScTableHead = styled.thead`
  text-align: left;
  th {
    font-weight: 400;
    font-size: 14px;
    line-height: 14px;
    opacity: 0.5;
  }
`;

export const ScTableBody = styled.tbody`
  td {
    font-weight: 500;
    font-size: 18px;
    line-height: 18px;
  }
`;
