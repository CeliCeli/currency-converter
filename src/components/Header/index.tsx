import { FC } from "react";
import { ScContainer } from "styles/general";
import { SсHeader } from "./styled";

export const Header: FC<{ array: string[][], date:  string }> = ({ array, date }) => {
  return (
    <SсHeader>
      <ScContainer>
        <a className="logo" href="/">
          <span>curr</span>ency.
        </a>
        <ul>
          <li className="date">{date}</li>
          {array.map(([el1, el2], index) => (
            <li key={index}>
              <span>{el1}</span>
              <span>{el2}</span>
            </li>
          ))}
        </ul>
      </ScContainer>
    </SсHeader>
  );
};
