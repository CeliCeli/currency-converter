import styled from "styled-components/macro";
import { theme } from "theme";

export const SсHeader = styled.div`
  background-color: ${theme.whiteColor};
  margin-bottom: 48px;
  padding: 30px 0;

  .logo {
    text-decoration: none;
    font-weight: 700;
    font-size: 20px;
    line-height: 20px;
    color: ${theme.secondaryColor1};

    span {
      color: ${theme.primaryColor};
    }
  }

  ul {
    display: flex;
    padding: 0;
    margin: 0;

    li {
      list-style: none;
      margin-left: 8px;
    }
    span {
      display: inline-block;
      opacity: .3;
      &:last-child {
        opacity: 1;
        margin-left: 4px;
      }
    }
  }
`;
