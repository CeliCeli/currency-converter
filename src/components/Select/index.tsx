import { FC, useState, useMemo, useContext } from "react";
import {
  ScSelect,
  ScOptionsList,
  ScOption,
  ScSelectedOption,
  ScSelectProps,
} from "./styled";
import {
  Currency,
  defaultValue,
  CurrencyContext,
  CurrenciesEnum,
} from "context/currency";

import { ReactComponent as Arrow } from "assets/images/arrow.svg";

interface SelectProps extends ScSelectProps {
  currency?: Currency[];
  value?: Currency;
}

export const Select: FC<SelectProps> = ({
  value = defaultValue,
  currency,
  maxWidth,
}) => {
  const [currentValue, setValue] = useState<Currency>(value);
  const [isOpen, setIsOpen] = useState(false);

  const { selectValue } = useContext(CurrencyContext);

  const handleSetValue = (currentValue: Currency) => {
    setValue(currentValue);
    selectValue(currentValue?.cc || CurrenciesEnum.UAH);
    setIsOpen((s) => !s);
  };

  const data = useMemo(() => {
    if (currency === null || !currency) return [currentValue];
    return currency.filter((arr) => arr.cc !== currentValue.cc);
  }, [currency, currentValue]);

  return (
    <ScSelect maxWidth={maxWidth}>
      <ScSelectedOption isOpen={isOpen} onClick={() => setIsOpen((s) => !s)}>
        <span>{currentValue.cc}</span>
        <img className="flag" src={currentValue.flag} alt="" />
        <Arrow />
      </ScSelectedOption>
      {isOpen && (
        <ScOptionsList>
          {data.map((obj, index) => (
            <li key={index}>
              <ScOption key={index} onClick={() => handleSetValue(obj)}>
                <span>{obj.cc}</span>
                <img className="flag" src={obj.flag} alt="" />
              </ScOption>
            </li>
          ))}
        </ScOptionsList>
      )}
    </ScSelect>
  );
};
