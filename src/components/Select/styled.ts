import styled, { css, keyframes } from "styled-components/macro";
import { theme } from "theme";

const show = keyframes`
    to{
        opacity: 1;

    }
`;

const spin = keyframes`
    50%{
        scale: -1 1;
    }
    100%{
        scale: 1 -1;

    }
`;

export interface ScSelectProps {
  maxWidth?: number;
}

export const ScSelect = styled.div<ScSelectProps>`
  position: relative;
  font-family: "Epilogue", sans-serif;
  min-width: 120px;
  max-width: ${({ maxWidth }) => `${maxWidth}px` ?? "100%"};
`;

export const ScOptionsList = styled.ul`
  padding: 2px;
  margin: 0;
  left: -2px;
  right: -2px;
  top: 52px;
  position: absolute;
  border-radius: 8px;
  background-color: ${theme.whiteColor};
  opacity: 0;
  z-index: 99;

  animation: ${show} 0.2s ease forwards;

  li {
    list-style: none;
    display: block;
    margin-bottom: 2px;
  }
`;

export const ScSelectedOption = styled.button<{ isOpen?: boolean }>`
  position: relative;
  border: 1px solid ${theme.secondaryColor2};
  background-color: transparent;
  border-radius: 8px;
  padding: 12px 11px;
  display: flex;
  align-items: center;
  cursor: pointer;
  transition: ease 0.3s;
  width: 100%;
  min-height: 50px;

  span {
    font-weight: 500;
    font-size: 14px;
    line-height: 130%;
    text-transform: uppercase;
    margin-right: 6px;
    max-width: 30px;

    &.placeholder {
      text-transform: capitalize;
      opacity: 0.3;
      position: absolute;
      left: 0;
      right: 0;
      top: 50%;
      translate: 0 -50%;
      max-width: 100%;
    }
  }

  .flag {
    width: 40px;
    height: 24px;
    object-fit: cover;
    border-radius: 2px;
    margin-right: 8px;
  }
  svg {
    transition: ease 0.3s;
  }

  &:hover {
    background-color: ${theme.secondaryColor2};
  }

  ${({ isOpen }) =>
    isOpen &&
    css`
      svg {
        animation: ${spin} 0.6s ease forwards;
      }
    `}
`;

export const ScOption = styled(ScSelectedOption)`
  cursor: pointer;
`;
