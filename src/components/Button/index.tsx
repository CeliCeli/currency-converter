import { FC } from "react";
import { ScButton, ScButtonProps } from "./styled";

import { ReactComponent as Triangle } from "assets/images/triangle.svg";

interface ButtonProps extends ScButtonProps {
  children: JSX.Element | string;
  onClick?: () => void;
}

export const Button: FC<ButtonProps> = ({
  children,
  onClick,
  withArrow,
  primary,
  ...rest
}) => {
  return (
    <ScButton type={"button"} primary={primary} onClick={onClick} withArrow={withArrow} {...rest}>
      {withArrow && <Triangle />}
      {children}
    </ScButton>
  );
};
