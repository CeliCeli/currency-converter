import styled, { css } from "styled-components/macro";
import { hexToRGB } from "helpers/hexToRgb";
import { theme } from "theme";

export interface ScButtonProps {
  backgroun?: string;
  height?: number | "auto";
  maxWidth?: number;

  primary?: boolean;

  withArrow?: boolean;

  clear?: boolean;
}
export const ScButton = styled.button<ScButtonProps>`
  border: none;
  cursor: pointer;
  transition: 0.3s;
  outline: none;
  font-weight: 600;
  font-size: 18px;
  line-height: 130%;
  position: relative;
  padding: ${({ clear }) => (clear ? "0" : "0 32px")};

  color: #1b1b1b;

  height: ${({ height }) =>
    height ? `${typeof height === "number" ? `${height}px` : height}` : "63px"};

  max-width: ${({ maxWidth }) => maxWidth && `${maxWidth}px`};

  background-color: ${({ backgroun }) =>
    backgroun ? hexToRGB(backgroun) : "transparent"};

  ${({ withArrow }) =>
    withArrow &&
    css`
      display: flex;
      align-items: center;
      svg {
        margin-right: 10px;
        transition: 0.3s;
      }
      &:hover {
        svg {
          translate: 5px;
        }
      }
    `}

  ${({ primary }) =>
    primary &&
    css`
      background-color: ${hexToRGB(theme.primaryColor)};
      &:hover {
        background-color: ${hexToRGB(theme.parimaryHover)};
      }
    `}

    &:hover {
    background-color: ${({ backgroun }) =>
      backgroun && hexToRGB(backgroun, 0.7)};
  }
`;
