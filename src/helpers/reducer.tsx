import { Currency } from "context/currency";

export enum ReducerTypes {
  SET_VALUE,
}

export enum ValueKeys {
  Value1 = "value1",
  Value2 = "value1",
}

type Value = {
  value1?: string;
  value2?: string;
};

type ReducerState = {
  valueReference: ValueKeys | "";
  values: Value;
  selectedCurrencyRate: Currency;
};

type Action = {
  type: ReducerTypes;
  payload: any;
};

function reducer(state: ReducerState, action: Action): ReducerState {
  const calc = Number(state.selectedCurrencyRate.rate * action.payload.value)
    .toFixed(2)
    .toString();

  switch (action.type) {
    case ReducerTypes.SET_VALUE:
      return {
        ...state,
        [action.payload.name]: action.payload.value,
        selectedCurrencyRate: action.payload.selectedCurrencyRate,
        values: {
          value1: action.payload.value,
          value2: calc,
        },
      };

    default:
      return state;
  }
}

export default reducer;
