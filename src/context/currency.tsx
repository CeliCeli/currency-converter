import {
  useState,
  FC,
  useEffect,
  ReactNode,
  useMemo,
  createContext,
  useReducer,
} from "react";

export enum CurrenciesEnum {
  USD = "USD",
  EUR = "EUR",
  UAH = "UAH",
  MXN = "MXN",
  PLN = "PLN",
  AUD = "AUD",
}

export type Currency = {
  cc: CurrenciesEnum;
  rate: number;
  flag: string;
  exchangedate?: string;
};

const currenciesFalgsPath: { [key in CurrenciesEnum]: string } = {
  [CurrenciesEnum.USD]: "https://www.countryflagicons.com/FLAT/64/US.png",
  [CurrenciesEnum.EUR]: "https://www.countryflagicons.com/FLAT/64/EU.png",
  [CurrenciesEnum.UAH]: "https://www.countryflagicons.com/FLAT/64/UA.png",
  [CurrenciesEnum.MXN]: "https://www.countryflagicons.com/FLAT/64/MX.png",
  [CurrenciesEnum.PLN]: "https://www.countryflagicons.com/FLAT/64/PL.png",
  [CurrenciesEnum.AUD]: "https://www.countryflagicons.com/FLAT/64/AU.png",
};

export const defaultValue: Currency = {
  cc: CurrenciesEnum.UAH,
  rate: 1,
  flag: currenciesFalgsPath[CurrenciesEnum.UAH],
};

interface CurrencyContextProps {
  array: Currency[];
  selectValue: (item: CurrenciesEnum) => void;
  selectedCurrency: Currency;
  dateFormApi: string;
}

export const CurrencyContext = createContext<CurrencyContextProps>({
  array: [defaultValue],
  dateFormApi: "",
  selectValue: () => {},
  selectedCurrency: defaultValue,
});

export const CurrencyProvider: FC<{ children?: ReactNode }> = ({
  children,
}) => {
  const [res, setData] = useState<Currency[]>([]);
  const [selectedCurrency, setSelectedCurrency] =
    useState<Currency>(defaultValue);

  useEffect(() => {
    if (res.length !== 0) return;

    const req = async () => {
      fetch("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json")
        .then((response) => response.json())
        .then((data) => setData(data));
    };

    req();
  }, [res]);

  const array: Currency[] = useMemo(() => {
    if (!res || res == null) return [];
    const arr: Currency[] = res
      .filter((x) => x.cc === CurrenciesEnum[`${x.cc ?? "UAH"}`])
      .map(({ cc, rate }) => {
        return {
          cc: cc,
          rate: rate,
          flag: currenciesFalgsPath[CurrenciesEnum[`${cc ?? "UAH"}`]],
        };
      });
    return [defaultValue, ...arr];
  }, [res]);

  const selectValue = (item: CurrenciesEnum) => {
    const selected = array.find((obj) => obj.cc === item);
    setSelectedCurrency(selected || defaultValue);
  };

  const dateFormApi: string = useMemo(() => {
    if (!res || !res[0]?.exchangedate) return "...";
    return res[0].exchangedate.toString();
  }, [res]);

  const value = useMemo(() => {
    return { array, selectValue, selectedCurrency, dateFormApi };
  }, [array, selectValue, dateFormApi, selectedCurrency]);

  return (
    <CurrencyContext.Provider value={value}>
      {children}
    </CurrencyContext.Provider>
  );
};
